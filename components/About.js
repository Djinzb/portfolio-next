import React, { Component } from "react";
import styled from "styled-components";
import { Controller, Scene } from "react-scrollmagic";
import { Tween, Timeline } from "react-gsap";
import TimelineCV from './Timeline';

const AboutPage = styled.div`
  width: 100vw;
  height: 100vh;
  position: relative;
  display: flex;
  align-items: center;
  flex-direction: column;
  section {
    position: absolute;
  }
  /* background: url("./static/svg-about/bg-about-4.svg") no-repeat bottom; */
  @media only screen and (max-width: 1080px) {
    background: url("./static/svg-about/bg-about-mobile-4.svg") no-repeat bottom;
  }
  @media only screen and (min-width: 1080px) {
    background: url("./static/svg-about/bg-about-6.svg") no-repeat bottom;
    background-size: cover;
  }
  
`;

const AboutImage1 = styled.div`
  width: 150px;
  height: 225px;
  border-radius: 7px;
  background: url("./static/Jens.png") no-repeat center;
  background-size: cover;
  margin: 0 auto;
`;

const AboutImage2 = styled.div`
  width: 150px;
  height: 225px;
  border-radius: 7px;
  background: url("./static/Jens-travel.png") no-repeat center;
  background-size: cover;
  margin: 0 auto;
`;

const AboutText = styled.div`
  width: 50vw;
  position: absolute;
  left: 15px;
  bottom: 15px;
  line-height: 1;
  letter-spacing: 1.5px;
  font-size: 1rem;
  text-align: justify;
`;

export default class About extends Component {
  render() {
    return (
      <div>
        <React.Fragment>
          <Controller>
            <Scene triggerHook="onLeave" duration={3000} pin >
              {progress => (
                <AboutPage>
                  <Timeline totalProgress={progress} paused>
                    <Timeline
                      target={
                        <section>
                          <h3 className="subtitle">Who I am</h3>
                          <AboutImage1 />
                        </section>
                      }
                    >
                      <Tween to={{ y: '0' }} />
                        <Timeline
                          target={
                              <AboutText>
                                Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua.
                              </AboutText>
                          }
                        >
                          <Tween from={{ y: '100', opacity: 0 }} to={{ y: '0', opacity: 1 }} />
                          <Tween to={{y:'-300', opacity: 0}} position="+=2" />
                        </Timeline>
                    <Tween to={{ y:'-200', opacity: 0 }} position="-=1"/>  
                    </Timeline>
                    
                    
                    <Timeline
                      target={
                        <section>
                          <h3 className="subtitle">What I love</h3>
                          <AboutImage2 />
                        </section>
                      }
                    >
                      <Tween from={{ y: '500', opacity: 0 }} to={{ y: '0', opacity: 1 }} />
                        <Timeline
                          target={
                              <AboutText>
                                Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua.
                              </AboutText>
                          }
                        >
                          <Tween from={{ y: '100', opacity: 0 }} to={{ y: '0', opacity: 1 }} />

                        </Timeline> 
                    </Timeline>

                  </Timeline>
                </AboutPage>
              )}
            </Scene>
          </Controller>
        </React.Fragment>

        <React.Fragment>
          <TimelineCV />
        </React.Fragment>
      </div>
    );
  }
}
