import React, { Component } from "react";
import styled from "styled-components";

export default class TimelineItemList extends Component {
  render() {
    const items = [
      {
        id: 1,
        title: "project 1",
        date: "1990",
        text: "blabla"
      },
      {
        id: 2,
        title: "project 2",
        date: "2000",
        text: "blabla"
      },
      {
        id: 3,
        title: "project 3",
        date: "2018",
        text: "blabla"
      }
    ];

    const TimelineItemList = styled.ul`
      list-style: none;
      margin: 0;
      padding: 0;
      position: relative;
    `;   

    return (
      <TimelineItemList>
        {items.map(item => {
          return (
            <li key={item.id}>
              <span />
              <div className="timeline-item-title">{item.title}</div>
              <div className="timeline-item-text">{item.text}</div>
              <div className="date">
                <span>{item.date}</span>
              </div>
            </li>
          );
        })}
      </TimelineItemList>
    );
  }
}
