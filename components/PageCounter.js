import React, { Component } from "react";
import { page } from "@stickyroll/decorators";
import Link from "next/link";

class PageCounter extends Component {
  render() {

    return (
      <React.Fragment>
        <strong>{this.props.page}</strong> of{" "}
        <strong>{this.props.pages}</strong>
      </React.Fragment>
    );
  }
}

export default page(PageCounter);
