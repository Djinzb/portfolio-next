import React, { Component } from "react";
import styled from "styled-components";

const TimelinePage = styled.div`
  width: 100vw;
  background-color: #252827;
  background: url('./static/svg-about/timeline-bg-top.svg')no-repeat top;
  color: white;
  .timeline {
    max-width: 900px;
    margin: 0 auto;
    padding: 0 10px 10px 10px;

    .timeline-body {
      position: relative;
      background-color: #2F3331;
      margin-left: 60px;
      border-radius: 0 8px 8px 0;
      padding: 5px 0;

      &:after {
        content: "";
        width: 4px;
        height: 100%;
        background-image: linear-gradient(to top left, #FF0000, #FFF500);
        position: absolute;
        left: -4px;
        top: 0;
      }

      .timeline-item {
        position: relative;

        &:after {
          content: "";
          width: 20px;
          height: 20px;
          border-radius: 50%;
          border: 4px solid #FFF500;
          background-color: #2F3331;
          position: absolute;
          left: -12px;
          top: 6px;
          z-index: 9;
        }

        .time {
          position: absolute;
          left: -60px;
          top: 4px;
          opacity: 0.5;
          font-size: 13px;
        }

        .content {
          margin: 40px;
          padding-bottom: 40px;
          border-bottom: 1px dashed #555;

          .title {
            text-transform: uppercase;
            margin-bottom: 15px;
          }
          
          p {
            line-height: 1;
            text-align: justify;
            font-size: 1rem;
          }
        }
      }
    }
  }
`;

export default class TimelineCV extends Component {
  render() {
    return (
      <TimelinePage>
        <div class="timeline">
          <div class="timeline-body">
            <div class="timeline-item">
              <p class="time">Aug 30</p>
              <div class="content">
                <h2 class="title">Title 1</h2>
                <p>
                  Lorem ipsum dolor sit amet, consect adicing.
                  Nesciunt eum facilis quae doloribus ea autem est debitis
                  veritatis reiciendis obcaecati, iure, voluptates ducimus
                  perspiciatis quo dolore quas? Provident error, voluptatum!
                </p>
              </div>
            </div>
            <div class="timeline-item">
              <p class="time">Aug 30</p>
              <div class="content">
                <h2 class="title">Title 2</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Nesciunt eum facilis quae doloribus ea autem est debitis
                </p>
              </div>
            </div>
            <div class="timeline-item">
              <p class="time">Aug 30</p>
              <div class="content">
                <h2 class="title">Title 3</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Nesciunt eum facilis quae doloribus ea autem est debitis
                </p>
              </div>
            </div>
            <div class="timeline-item">
              <p class="time">Aug 30</p>
              <div class="content">
                <h2 class="title">Title 4</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Nesciunt eum facilis quae doloribus ea autem est debitis
                </p>
              </div>
            </div>
            <div class="timeline-item">
              <p class="time">Aug 30</p>
              <div class="content">
                <h2 class="title">Title 5</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Nesciunt eum facilis quae doloribus ea autem est debitis
                </p>
              </div>
            </div>
          </div>
        </div>
      </TimelinePage>
    );
  }
}
