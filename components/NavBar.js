import React, { Component } from "react";
import Link from "next/link";
import styled from "styled-components";
import posed from 'react-pose';

const NavAnimation = posed.nav({
  hoverable: true,
  init: { height: 30 },
  hover: { height: 75 }
})

const Nav = styled(NavAnimation)`
  width: 100vw;
  height: 30px;
  border: 1px solid grey;
  border-radius: 5px 5px 0 0;
  position: absolute;
  bottom: 0;
  background-color: rgba(255, 255, 255, 0.6);
`;

const NavPullUpItem = styled.div`
  width: 100%;
  height: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

const NavPullUpIcon = styled.span`
  border: 2px solid grey;
  border-radius: 20px;
  width: 30px;
`;

const NavLinkIcon = styled.span`
  transform: rotate(-90deg);
  width: 25px;
  height: 25px;
  border: 1px solid;
  border-radius: 5px;
`;

export default class NavBar extends Component {

  state = { open: false };

  render() {
    const { open } = this.state;

    return (
      <Nav >
        <NavPullUpItem><NavPullUpIcon /></NavPullUpItem>
        <Link href="/"><NavLinkIcon>H</NavLinkIcon></Link>
        <NavLinkIcon><Link href="/about">A</Link></NavLinkIcon>
        <NavLinkIcon><Link href="/timeline">T</Link></NavLinkIcon>
      </Nav>
    );
  }
}
