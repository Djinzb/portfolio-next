import React, { Component } from "react";
import styled from "styled-components";

const AboutCardHeader = styled.h3`
  font-size: 1rem;
`;

const Card = styled.div`
  height: 80vh;
  border: 1px solid black;
`;


export default class About3 extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="col">
          <Card>
            <AboutCardHeader className="subtitle">About 3</AboutCardHeader>
          </Card>
        </div>
      </React.Fragment>
    );
  }
}
