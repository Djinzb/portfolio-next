import React, { Component } from "react";
import styled from "styled-components";
import { FiUserCheck, FiActivity, FiCompass } from "react-icons/fi"

const AboutCardHeader = styled.h3`
  font-size: 1rem;
`;

const Card = styled.div`
  height: 80vh;
  border: 1px solid black;
`;

export default class About1 extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="col">
          <Card>
            <AboutCardHeader className="subtitle">About 1</AboutCardHeader>
          </Card>
        </div>
      </React.Fragment>
    );
  }
}
