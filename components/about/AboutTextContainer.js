import styled from "styled-components";

const AboutTextContainer = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: rgba(255, 255, 255, 0.6);
  text-align: center;
`;

export default AboutTextContainer;