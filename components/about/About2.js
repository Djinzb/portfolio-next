import React, { Component } from "react";
import styled from "styled-components";
import AboutTextContainer from "./AboutTextContainer";
import ScrollAnimation from "react-animate-on-scroll";

const AboutContainer2 = styled.div`
  width: 100vw;
  height: 100vh;
  background: url("../static/Jens-sports.png") no-repeat center center;
  background-size: contain;
`;

const AboutCardHeader = styled.h3`
  font-size: 1rem;
`;

const Card = styled.div`
  height: 80vh;
  border: 1px solid black;
`;

export default class About2 extends Component {
  render() {
    return (
      <React.Fragment>
      <div className="col">
        <Card>
          <AboutCardHeader className="subtitle">About 2</AboutCardHeader>
        </Card>
      </div>
    </React.Fragment>
    );
  }
}
