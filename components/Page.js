import React, { Component } from "react";
import Meta from "../components/Meta";
import Header from "./Header";
import styled, { ThemeProvider, injectGlobal } from "styled-components";
import NavBar from "./NavBar";

// theme voor styled components = strings die doorgegeven worden
// doorgegeven in props ThemeProvider in render
const theme = {
  red: "#FF0000",
  black: "#393939",
  grey: "#3A3A3A",
  lightgrey: "#E1E1E1",
  offWhite: "#EDEDED",
  maxWidth: "1000px",
  bs: "0 12px 24px 0 rgba(0, 0, 0, 0.09)"
};

// styled component
const StyledPage = styled.div`
  background: white;
  color: ${props => props.theme.black};
`;
// styled component
const Inner = styled.div`
  margin: 0 auto;
`;

injectGlobal`
  @font-face {
    font-family: 'SF';
    src: url('/static/SF-regular.otf');
    font-weight: normal;
    font-style: normal;
  }
  html {
    box-sizing: border-box;
    font-size: 15px;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    padding: 0;
    margin: 0;
    font-size: 1.5rem;
    line-height: 2;
    font-family: 'SF';
    overflow-y: auto !important;
  }
  a {
    text-decoration: none;
    color: ${theme.black};
  }
  
  button {  font-family: 'SF'; }
  fieldset {
    border: 1px solid grey;
    border-radius: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 50vh;
    box-shadow: 0 12px 24px 0 rgba(0, 0, 0, 0.09);
    
    input {
      padding: 10px;
      margin: 10px;
    }
    button {
      padding: 10px;
      margin: 15px auto;
    }
  }
  .subtitle {
    text-transform: uppercase;
    font-weight: 100;
    padding: 0;
    margin: 0 auto;
    font-size: 2rem;
    letter-spacing: 3px;
  }

`;

export default class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <StyledPage>
          <Meta />
          <Header />
            {/* <NavBar /> */}
            {this.props.children}
        </StyledPage>
      </ThemeProvider>
    );
  }
}
