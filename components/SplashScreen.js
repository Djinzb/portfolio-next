import React, { Component } from "react";
import styled from "styled-components";
import Link from "next/link";
import Router from 'next/router';

const Knockout = styled.div`
  .landingpage {
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    position: fixed;
    background-color: white;
    z-index: 10;
  }
  .backdrop {
    background: url("../static/bg2.gif") no-repeat center;
    background-size: cover;
    margin: 0 auto;
    width: 100vw;
  }
  .backdrop.zoom {
    transform: scale(10);
    opacity: 0;
    -webkit-transition: all 1s ease-in-out;
    -moz-transition: all 1s ease-in-out;
    -ms-transition: all 1s ease-in-out;
    -o-transition: all 1s ease-in-out;
    transition: all 1s ease-in-out;
  }
  @media screen and (max-width: 800px) {
    .text {
      font-size: 4rem;
    }
  }
  @media screen and (min-width: 800px) {
    .text {
      font-size: 4rem;
    }
  }
  .text {
    font-family: "SF";
    font-weight: bold;
    text-align: center;
    line-height: 90%;
    margin: 0;

    small {
      font-size: 1.5rem;
    }
  }
  .knockout {
    color: black;
    mix-blend-mode: lighten;
    background-color: white;
    margin: -1px auto;
  }
  .show {
    opacity: 1;
  }
  .hide {
    display: none;
  }
`;

export default class SplashScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.handleClickAnimation();
    }, 2000);
    Router.prefetch('/about');
  }


  handleClickAnimation = event => {
    const backdrop = document.getElementsByClassName("backdrop")[0];
    const landingpage = document.getElementsByClassName("landingpage")[0];

    backdrop.classList.add("zoom");
    backdrop.addEventListener("transitionend", event => {
      backdrop.classList.add("hide");
      landingpage.classList.add("hide");
      Router.push('/about')
    });
  };

  render() {
    return (
        <Knockout>
          <div className="landingpage page">
            <div className="backdrop">
              <h1 className="text knockout">
                JENS BUYST
                <br />
                <small>WEB DEVELOPER</small>
              </h1>
            </div>
          </div>
        </Knockout>
    );
  }
}
